#!/usr/bin/env nextflow

echo true
sequencias_aln = Channel.fromPath('../../Aln_Tree/sequencias_aln.fasta')

process alignment{

	input:
	file sequencias_aln
	
	output:
	file seq_aligned
	"""
	mafft --auto sequencias_juntas.fasta
	"""
	
}
	 
process raxml{
	
	input:
	file sequencias_juntas.fasta from seq_aligned

	"""
	raxmlHPC-PTHREADS-AVX -T 6 -f a -m GTRCAT -p 258369 -x 258369 -N 50 -s
	"""

}
	


