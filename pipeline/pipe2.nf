#!/usr/bin/env nextflow

echo true
params.level = ""
params.species = ""

/*
*MUDAR AS VARIAVEIS COMO PARAMETROS DESNECESSARIAS
*/
unaligned_seqs = file("./Files/unaligned/seq1.fasta")
ncbiSeq = file('./ncbi_data/obtain_species_ncbi.py')
filter = file("./Files/1.get_all_the_genes_of_the_organism.py")





/*
*process aligner {
*    
*    publishDir './Files/aligned' , mode: 'copy'
*	
*    input:
*    file 'seq1.fasta' from unaligned_seqs
* 
*    output:
*    file 'seq1_aln.fasta' into alignments
* 
*    """
*    mafft --auto seq1.fasta > seq1_aln.fasta
*    """
* 
*}
*
*process treebuilder {
*	input:
*	file x from alignments
*
*
*	"""
*	raxmlHPC-PTHREADS-AVX -T 6 -f a -m GTRCAT -p 258369 -x 258369 -N *50 -s $outseq -n seq_aln_out_raxml 
*	"""
*}
*
*
*	
*
*
*result.subscribe { println it }
*
*process mafft{
*	input:
*	file sequencias_juntas.fasta
*	
*
*	"""
*	mafft --auto sequencias_juntas.fasta > sequencias_aln.fasta
*	cp sequencias_aln.fasta ../../../Files
*	echo "Mafft process executed successfuly"
*	
*	"""
*}
*/
process scriptcare{

	input:
	file 'obtain_species_ncbi.py' from ncbiSeq 
 
	output:
	stdout dict
 
    	script:
	if ("$params.species" == "")
		error "Species not provided, specify one by --species 'value'"

	else if ("$params.level" == "")
				
		error "Species taxonomic rank not provided, specify one by --level 'value'"
	
			
	else
		"""
   		python3 $ncbiSeq "${params.species}" "${params.level}"
    		"""	
}

/*process taxaFilter{
*	input:
*	stdin dict
*
*	"""
*	python3 $filter
*	"""
*
*
*}
*/
