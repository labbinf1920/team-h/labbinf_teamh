"""!/usr/bin/env python3"""
import argparse
from Bio import Entrez
from Bio import SeqIO
import pandas as pd


# Part 1
def add_gene(ind, dict_gene_local, list_names_features, seq_record):
    """
    The purpose of this function is to add a new gene to the dictionary (dict_gene_local)
    :param ind: string (name) to find in the list list_names_features
    :param dict_gene_local: dictionary as the name of the genes as keys and the repetitive
    numbers of access and location
    :param list_names_features: list with all features
    :param seq_record: item in GenBank format
    :return: Updated dictionary with the new gene.
    """
    index = list_names_features.index(ind)
    dict_qualifiers = seq_record.features[index].qualifiers
    try:
        # /gene=  gene name
        unofficial_gene_name = dict_qualifiers["gene"]
    except KeyError:
        try:
            # /product=  product name
            unofficial_gene_name = dict_qualifiers["product"]
        except KeyError:
            try:
                # /locus_tag= gene name in additional information
                unofficial_gene_name = dict_qualifiers["locus_tag"]
            except KeyError:
                # gene doesn't exist or does not want to be added to the dictionary
                return dict_gene_local

    if unofficial_gene_name[0] not in list(dict_gene_local.keys()):
        dict_gene_local[unofficial_gene_name[0]] = []
    location = seq_record.features[index].location  # type location:    <1..>1532

    start_local = int(str(location.start).replace("<", ""))  # location start = "<1"
    end_local = int(str(location.end).replace(">", ""))  # location end =   ">1532"

    dict_gene_local[unofficial_gene_name[0]].append([seq_record.id, start_local, end_local])

    return dict_gene_local


def shorter_or_longer_sequence(dict_gene_local, location):
    """
    :param dict_gene_local: Dictionary with the name of the
    genes, the access numbers and the location
    :param location: Gene location
    :return: Dictionary with the name of the gene, the access
    and location numbers, of the largest or smallest gene
    """
    dict_b_s = {}

    for gene_key in dict_gene_local.keys():
        list_b_s = []
        for start_end in dict_gene_local[gene_key]:
            start_local = start_end[1]

            end_local = start_end[2]
            list_b_s.append(end_local - start_local)
        index_p = list_b_s.index(sorted(list_b_s)[location])
        dict_b_s[gene_key] = dict_gene_local[gene_key][index_p]

    return dict_b_s


def first_or_last_sequence(dict_gene_local, local_list):
    """

    :param dict_gene_local: Dictionary with the name of
    the genes, the access numbers and the location
    :param local_list: local in list of keys
    :return: Dictionary with the name of the gene, the access
    and location numbers, of the first or last gene
    """

    dict_new = {}
    for keyy in dict_gene_local.keys():
        dict_new[keyy] = dict_gene_local[keyy][local_list]

    return dict_new


def get_all_gene_organism(organism_name, option):
    """
    :param organism_name: Organism name (scientific name)
    :param option: Type of sequence
    :return:
    """
    hand = Entrez.esearch(db="nucleotide", term=organism_name + "[organism]", usehistory="y")
    rec = Entrez.read(hand)
    hand.close()
    # limit < 10.000 because the function efetch() can only receive a retmax up to 10000
    limit = 100
    dict_gene_local = {}

    for retstart in range(0, int(rec["Count"]), limit):
        with Entrez.efetch(db="nucleotide", rettype="gb",
                           retmode="text", usehistory="y",
                           retstart=retstart, retmax=limit,
                           webenv=rec["WebEnv"], query_key=rec["QueryKey"]) as handle:

            for seq_record in SeqIO.parse(handle, "gb"):
                list_names_features = [seq_record.features[j].type
                                       for j in range(len(seq_record.features))]
                n_gene = list_names_features.count("gene")

                if n_gene == 0 or n_gene == 1:
                    if "gene" in list_names_features:
                        dict_gene_local = add_gene("gene", dict_gene_local,
                                                   list_names_features, seq_record)
                    elif "CDS" in list_names_features:
                        dict_gene_local = add_gene("CDS", dict_gene_local,
                                                   list_names_features, seq_record)
                    elif "mRNA" in list_names_features:
                        dict_gene_local = add_gene("mRNA", dict_gene_local,
                                                   list_names_features, seq_record)
                    elif "rRNA" in list_names_features:
                        dict_gene_local = add_gene("rRNA", dict_gene_local,
                                                   list_names_features, seq_record)
                    elif "tRNA" in list_names_features:
                        dict_gene_local = add_gene("tRNA", dict_gene_local,
                                                   list_names_features, seq_record)
                    elif "misc_feature" in list_names_features:
                        dict_gene_local = add_gene("misc_feature", dict_gene_local,
                                                   list_names_features, seq_record)
                    else:
                        pass

                if n_gene > 1:
                    list_t = [indice for indice in range(len(list_names_features))
                              if list_names_features[indice] == "gene"]

                    for index in list_t:
                        dict_gene_local = add_gene(list_names_features[index],
                                                   dict_gene_local,
                                                   list_names_features,
                                                   seq_record)

    if option == "b":
        return shorter_or_longer_sequence(dict_gene_local, 0)  # Bigger
    elif option == "s":
        return shorter_or_longer_sequence(dict_gene_local, -1)  # Smaller
    elif option == "f":
        return first_or_last_sequence(dict_gene_local, 0)  # First
    else:  # option == "-l":
        return first_or_last_sequence(dict_gene_local, -1)  # Last or others


# Part 2
def get_sequence_from_accession_number(gene):
    """
    The purpose of this function is to find out the official name of the gene
    :param gene: Gene name
    :return: Official gene name
    """

    hand = Entrez.esearch(db="gene", term=gene + "[sym]", usehistory="y", retmax=10000)
    rec = Entrez.read(hand)
    hand.close()

    list_sym = []
    # NCBI Summary (text) format information
    with Entrez.efetch(db="gene", retmode="text", usehistory="y", webenv=rec["WebEnv"],
                       query_key=rec["QueryKey"]) as handle:

        gene_information = str(handle.read())[1:][:-2]

        # if "<ERROR>" exists, it means that the gene does not exist
        if "<ERROR>" not in gene_information:
            for piece_data in gene_information.split("\n\n"):
                line = piece_data.split("\n")
                if line[0] == "":
                    line.pop(0)

                else:  # line[0] != "":
                    # list with the official names of the gene
                    list_sym.append(line[0].split(". ")[1])

    official_name = ""
    counter = 0
    # This "for" returns the official name more often
    for unique_gene_name in set(list_sym):
        if list_sym.count(unique_gene_name) > counter:
            counter = list_sym.count(unique_gene_name)
            official_name = unique_gene_name

    return official_name.lower()


# Part 3
def delete_missing_data(dataframe, missing_data_limit):
    """
    :param dataframe: Dataframe with the names of the genes and the names of the organisms
    :param missing_data_limit: Missing data limit value
    :return: New dataframe with less or equal number of columns
    """
    for column_name in dataframe.columns:
        percentage_null = dataframe[column_name].isnull().sum() / len(dataframe[column_name])
        if str(percentage_null) != str("nan"):
            if missing_data_limit < percentage_null:
                del dataframe[column_name]
    return dataframe


# Part 4
def get_sequence(acc_number, beg_sequence, end_sequence):
    """
    :param acc_number: Accession number in NCBI
    :param beg_sequence: start of sequence
    :param end_sequence: end of sequence
    :return:
    """
    if beg_sequence != 0:
        beg_sequence = beg_sequence - 1

    hand = Entrez.esearch(db="nucleotide", term=acc_number, usehistory="y")
    rec = Entrez.read(hand)
    hand.close()

    # NCBI fasta(text) format information
    with Entrez.efetch(db="nucleotide", rettype="fasta", retmode="text", usehistory="y",
                       webenv=rec["WebEnv"], query_key=rec["QueryKey"]) as handle:
        seq_record = handle.read()

        # if "<ERROR>" exists, it means that the accession number does not exist
        if "<ERROR>" not in seq_record:
            sequence = "".join(seq_record.split("\n")[1:])
            return sequence[beg_sequence:end_sequence]
        else:
            return ""


if __name__ == '__main__':
    Entrez.email = "rubenaalbquerque@hotmail.com"

    # Part 0 - Passing arguments with argparse
    PARSER = argparse.ArgumentParser()

    PARSER.add_argument('--Organism', '-org', type=str,
                        help='Organism name (scientific name)')
    PARSER.add_argument('--ListOrganisms', '-l', type=str,
                        help='List of organisms with the same taxonomic level')
    PARSER.add_argument('--ProportionOrganism', '-po', type=float,
                        help='Limit of missing data in the organism(optional)')
    PARSER.add_argument('--ProportionGene', '-pg', type=float,
                        help='Limit of missing data in the gene(optional)')
    PARSER.add_argument('--TypeSequence', '-ts', type=str,
                        help='Choose which type of sequence(optional): Bigger(B), '
                             'Shortest(S), Most recent/First (F), Least recent (L)')
    ARGS = PARSER.parse_args()

    LIST_WITH_ORGANISM_NAMES = eval(ARGS.ListOrganisms)

    if ARGS.ProportionGene:
        PROPORTIONGENE = ARGS.ProportionGene
    else:
        PROPORTIONGENE = 0.9

    if ARGS.ProportionOrganism:
        PROPORTIONORGANISM = ARGS.ProportionOrganism
    else:
        PROPORTIONORGANISM = 0.8

    if ARGS.TypeSequence:
        TYPESEQUENCE = ARGS.TypeSequence
    else:
        TYPESEQUENCE = "l"

    # Part 1 - Get all the genes from each organism
    DICT_ORG_GENES = {}
    for name in LIST_WITH_ORGANISM_NAMES:
        DICT_ORG_GENES[name] = get_all_gene_organism(name, TYPESEQUENCE)

    # Part 2 - Change the gene names to the official gene name
    DICT_ORG = {}
    for key, value in DICT_ORG_GENES.items():
        for k in value.copy():
            name_oficial = get_sequence_from_accession_number(k)
            value[name_oficial] = value.pop(k)
        DICT_ORG[key] = value

    # Part 3 - The user only accepts a certain percentage of NaN
    DF = pd.DataFrame(DICT_ORG)
    DF = delete_missing_data(DF, PROPORTIONORGANISM)
    DATAFRAME_CLEAN = delete_missing_data(DF.T, PROPORTIONGENE)

    # Part 4 - Create files with the name of the genes.
    #         - Each file contains the name of the organism and sequence.
    DICTDF = DATAFRAME_CLEAN.to_dict()

    for gene, dict_organism in DICTDF.items():
        organism_number_per_file = len([i for i in dict_organism.values()
                                        if str(i) != str("nan")])
        if organism_number_per_file > 1:
            for org_name, acc_local in dict_organism.items():
                if str(acc_local) != str("nan"):

                    acc = acc_local[0]
                    start = int(acc_local[1])
                    end = int(acc_local[2])
                    seq = get_sequence(acc, start, end)

                    if str(seq) != "":
                        for carater in ["\\", "|", "(", ")"]:
                            if carater in gene:
                                gene.replace(carater, "")
                        gene_name = gene.replace(" ", "_")
                        org_name = org_name.replace(" ", "_")

                        with open(gene_name + ".fasta", "a") as new_file:
                            new_file.write(">" + org_name + "\n" + seq + "\n\n")

