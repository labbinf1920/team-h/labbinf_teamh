#!/usr/bin/env python3

"""SCRIPT -> ML_tree_draw.py:
Input: raxml tree file
Output: png image of the tree"""

import sys
from ete3 import Tree, TreeStyle, NodeStyle, TextFace
#import PyQt5
from Bio import Phylo

def ml_tree_visualization(tree_file):
    """ This function allows the visualization of a Maximum Likelyhood """
    # -------------------------------------------------------------------------
    # Converter para o formato que é suportado pelo ete3
    Phylo.convert(tree_file, 'newick', "example.nh", 'newick')
    tree = Tree("example.nh") # Cria o objeto que contém a árvore
    # -------------------------------------------------------------------------
    # Manipula o aspeto da árvore através de um conjunto de opções
    tree_style = TreeStyle()
    # -------------------------------------------------------------------------
    # Adiciona um título à árvore
    tree_style.title.add_face(TextFace("Multigene Tree - ML", fsize=20), column=0)
    # -------------------------------------------------------------------------
    # Adiciona, à árvore, o nome de cada ramo
    tree_style.show_leaf_name = True
    # -------------------------------------------------------------------------
    # Adiciona, à árvore, o suporte dos ramos -> Valores de Bootstrap
    tree_style.show_branch_support = True
    # -------------------------------------------------------------------------
    # Aumenta o comprimento da árvore (Altera a branch lenght)
    tree_style.scale = 100
    # -------------------------------------------------------------------------
    # Aumentar a separação entre leaf branches
    tree_style.branch_vertical_margin = 30
    # -------------------------------------------------------------------------
    # Redefinir o aspeto dos branches
    nstyle = NodeStyle()
    nstyle["hz_line_width"] = 2
    nstyle["hz_line_type"] = 1
    nstyle["hz_line_color"] = "orangered"
    nstyle["vt_line_width"] = 2
    nstyle["vt_line_type"] = 0
    nstyle["vt_line_color"] = "black"
    # -------------------------------------------------------------------------
    # Redefinir o aspeto de todos os nodes presentes na árvore
    for node in tree.traverse():
        nstyle["size"] = 5 # Tamanho
        nstyle["fgcolor"] = "firebrick" # Cor
        node.set_style(nstyle)
    # -------------------------------------------------------------------------
    # Faz a renderização da árvore para uma imagem png, pdf ou svg
    tree.render("ML_tree.png", tree_style=tree_style, dpi=500, h=1920, w=1080, units='px')
    tree.render("mytree.svg", tree_style=tree_style, dpi=500, h=1920, w=1080, units='px')
    # h - altura
    # w - largura

    return 0

def main():
    """ Main Function """
    tree_file = sys.argv[1]
    print(tree_file)
    ml_tree_visualization(tree_file)

if __name__ == "__main__":
    main()
