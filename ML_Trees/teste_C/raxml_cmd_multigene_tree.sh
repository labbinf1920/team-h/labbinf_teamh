#!/bin/bash

# Alinhar os genes - o script está configurado para funcionar dentro da pasta teste_C
mafft --auto ./12sV2/12sV2.fas > ../teste_C/12sV2_aln.fasta

mafft --auto ./16sV2/16sV2.fas > ../teste_C/16sV2_aln.fasta

mafft --auto ./CytbV2/CytbV2.fas > ../teste_C/CytbV2_aln.fasta

mafft --auto ./ND4V2/ND4V2.fas > ../teste_C/ND4V2_aln.fasta


# Concatenar os ficheiros alinhados em um só - usei o script catfasta2phyml.pl
./catfasta2phyml.pl ./12sV2_aln.fasta ./16sV2_aln.fasta ./CytbV2_aln.fasta ./ND4V2_aln.fasta > genes_aln.fasta


# Uma vez concatenado, a árvore de ML é construida, através do raxml
raxmlHPC-PTHREADS-AVX -T $1 -f a -m $2 -p 513846 -x 513846 -N $3 -s ./genes_aln.fasta -n Multigene_tree_out_raxml
# $1 - número de threads
# $2 - modelo
# $3 - número de replicados






