#!/usr/bin/env python3

from ete3 import Tree, TreeStyle, NodeStyle, TextFace
#import PyQt5
from Bio import Phylo 


#no converter e no tree se for em windows tens de por o caminho do ficheiro
converter = Phylo.convert("RAxML_bestTree.seq_aln_out_raxml", 'newick', "example.nh", 'newick') # Converter para o formato que é suportado pelo ete3
t = Tree("example.nh") # Cria o objeto que contém a árvore


ts = TreeStyle() # Utilizado para criar um conjunto de opções que controlam o aspeto geral da árvore - Manipula o aspeto da árvore


ts.title.add_face(TextFace("Árvore de ML", fsize=20), column=0) # Adiciona um título à árvore
ts.show_leaf_name = True # Adiciona, à árvore, o nome de cada ramo 
ts.show_branch_length = True # Adiciona, à árvore, o comprimento dos ramos
ts.show_branch_support = True # Adiciona, à árvore, o suporte dos ramos
#ts.scale = 100 # Aumenta o comprimento da árvore (Altera a branch lenght)
ts.branch_vertical_margin = 25 # Aumentar a separação entre leaf branches


# Definir um outgroup
Rhya = t&"Rhyacichthys_aspro"
Rhya.set_style(NodeStyle())
Rhya.img_style["bgcolor"]="lightgreen"


# Redefinir o aspeto dos branches
nstyle = NodeStyle()
nstyle["hz_line_width"] = 2 
nstyle["hz_line_type"] = 1 
nstyle["hz_line_color"] = "orangered"

nstyle["vt_line_width"] = 2 
nstyle["vt_line_type"] = 0 
nstyle["vt_line_color"] = "black"


for node in t.traverse():
# Redefinir o aspeto de todos os nodes presentes na árvore
	nstyle["size"] = 10 # Tamanho
	nstyle["fgcolor"] = "firebrick" # Cor
	node.set_style(nstyle)


#t.render("mytree.png", w=183, units="mm") # Faz a renderização da árvore para uma imagem png, pdf ou svg

t.show(tree_style=ts) # Permite visualizar a árvore no standart Output, com o Tree Style definido
