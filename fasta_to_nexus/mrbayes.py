
"""
 This Script creates the end part of the nexus file
"""

def mrbayes_blockwriter(ngen):
    '''
        Recieves the argument ngen (integer).
	Returns a variable with the final text to include in the nexus file.
        :param ngen: recieves the variable ngen, in the integer
        :return: returns the variable with the final text
    '''
    end = "  ;\nEND;\n\n"

    # puts in a variable, the following string
    mrbayes = "begin mrbayes;\n  set autoclose=yes nowarn=yes quitonerror=no;\n" \
              "  mcmcp ngen={} printfre=1000 samplefreq=100 diagnfre=1000" \
              " nchains=4 savebrlens=yes filename=MyRun01;\n" \
              "  mcmc;" \
              "  sumt filename=MyRun01;\n" \
              "end;".format(ngen)

    #joins both strings in a variable
    mrbayes_final = end + mrbayes

    return mrbayes_final
