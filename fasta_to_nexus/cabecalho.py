"""
 Creates the beggining of the nexus file
"""

def cabecalho(fasta):
    '''
     Creates the beggining of the nexus files, calculating in this function the number of taxas
     :parameter fasta: recieves the sequences and their names
     :return: returns the print that contains the beggining of the nexus file
    '''

    # Calculates the number of taxas using the number of existing keys
    taxa = 0
    for value in fasta.keys():
        taxa += 1

    # Calculates the total amount of Nucleotides, Missing and Gaps that exists in the sequence
    for values in fasta.values():
        nchar = len(values)
        break

    # Makes a "print" at the star of the nexus file, that contains the information
    # before the alignment of the Nexus sequences
    start_nexus = "#Nexus\n\nBEGIN DATA;\nDIMENSIONS NTAX={} " \
                  "NCHAR={};\nFORMAT DATATYPE=DNA MISSING=N GAP=-;" \
                  "\nMATRIX\n".format(taxa, nchar)

    return start_nexus
