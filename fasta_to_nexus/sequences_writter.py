
"""
 This script aligns all the names and sequences
"""

def seq_writter(fasta):
    '''
     Aligns all sequences
     :param fasta: Recieves the number of sequences and their names
     :return: Returns the aligned sequence
    '''

    sequences = ""

    #The variable "maior" will recieve the biggest name of all the sequences
    #If the name is bigger than 99 char, everything after that will be cut

    maior = ""
    for k, var in fasta.items():
        if len(k) > len(maior):
            maior = k
        if len(maior) >= 99:
            maior = maior[:99]

    # Checks all names and sequences and if the name is over 99 char, cuts everything in front
    # Aligns all names and sequences

    for k, var in fasta.items():
        if len(k) >= 99:
            k = k[:98]
        qualquer = len(maior) - 1
        tamanho = qualquer - len(k)
        sequences += " "*tamanho + "{}  {}".format(k, var) + "\n"

    return sequences
