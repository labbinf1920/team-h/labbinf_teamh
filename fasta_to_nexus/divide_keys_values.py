
"""
 divides all keys and values from the fasta file
"""

from itertools import groupby

def fasta_rearrange(fasta):
    '''
     Locates every number and sequences and puts it in a dictionary, using the fasta file as an argument.
    :param fasta: recieves the fasta file
    :return: the names of the sequences with the sequences
    '''

    with open(fasta, "r") as fasta_file:
        # searches the numbers started with >
        groups = groupby(fasta_file, key=lambda x: not x.startswith(">"))
        # creates an empty dictionary
        fasta_file = {}
        # for each value in the list, the \n is replaced with nothing
        for keys, values in groups:
            if not keys:
                key, val = list(values)[0].replace('\n', ''), "".join(map(str.rstrip, next(groups)[1]))
                # puts the values and keys in the dictionary
                fasta_file[key] = val
    # replaces > with a space
    novo = {keys.replace('>', ''): value for keys, value in fasta_file.items()}
    return novo
