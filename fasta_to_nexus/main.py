
"""
 Uses all of the other scripts to convert the fasta file to a nexus file
"""

from sys import argv
import cabecalho
import divide_keys_values
import mrbayes
import sequences_writter


# obtains the variables inputed from the user
FASTA_FILE_FROM_USER = argv[1]
NGEN = argv[2]


DKV = divide_keys_values.fasta_rearrange(FASTA_FILE_FROM_USER)
# runs the module 'divide_keys_values', while running the function'fasta_rearrange'
# while using the fasta file from input

START_NEXUS = cabecalho.cabecalho(DKV)
# runs the 'cabecalho', while running the function 'cabecalho'
# obtains the variable 'START_NEXUS'

SEQUENCES = sequences_writter.seq_writter(DKV)
# runs the module 'sequences_writter', while running the function 'seq_writter'

MRBAYES_LASTBLOCK = mrbayes.mrbayes_blockwriter(NGEN)
# runs the module 'mrbayes', while running the function 'mrbayes_blockwriter'

NEXUS_WRITTER = START_NEXUS + SEQUENCES + MRBAYES_LASTBLOCK
# the variable nexus_writter joins the multiple variables obtained earlier

NEXUS_FILE = open("nexus_file.nex", "w+")
# creates a nexus file

NEXUS_FILE.write(NEXUS_WRITTER)
# writes the variable 'nexus_writter' into the file

NEXUS_FILE.close()
# fecha o ficheiro
