#!/usr/bin/python

"""
 This script allows us to concatenate all fasta files into 1
"""

import os
import sys

def main():
    """
       Main function
    """

    if len(sys.argv) <= 1:
        print("No files provided!")
        print('Usage: ./concatFasta.py *.fasta - or - ./concatFasta.py 1.fas 2.fas...')
        sys.exit(1)
    files = sys.argv[1:]

    #print("concatenating fastas using the following order:")
    #print("if this is incorrect, change something")
    samps = dict()
    #loop through and get list of samples
    for file in sorted(files):
        for sample in read_fasta(file):
            samps[sample[0]] = ""
    for file in sorted(files):
        #print(file)
        #get seqlen
        seqlen = None
        #seen
        seen = dict()
        for seens in read_fasta(file):
            seen[seens[0]] = 0
            seqlen = len(seens[1])
            samps[seens[0]] = samps[seens[0]] + seens[1]
        for key in samps.keys():
            if key not in seen:
                samps[key] = samps[key] + nrepeats("N", seqlen)
    oname = "concat.fasta"
    write_fasta(oname, samps)


def nrepeats(pattern, slen):
    """
     Number of patterns in a sequence
    """
    ret = ""
    for _ in range(int(slen)):
        ret = ret + str(pattern)
    return ret


def write_fasta(name, samp):
    """
     write fasta from dict
    """
    with open(name, 'w') as fileh:
        try:
            for sample in samp.keys():
                to_write = ">" + str(sample) + "\n" + samp[sample] + "\n"
                fileh.write(to_write)
        except IOError as excpt:
            print("Could not read file:", excpt)
            sys.exit(1)
        except Exception as excpt:
            print("Unexpected error:", excpt)
            sys.exit(1)
        finally:
            fileh.close()


def read_fasta(fas):
    """
     Read samples as FASTA. Generator function
    """
    if os.path.exists(fas):
        with open(fas, 'r') as fileh:
            try:
                contig = ""
                seq = ""
                for line in fileh:
                    line = line.strip()
                    if not line:
                        continue
                    #print(line)
                    if line[0] == ">": #Found a header line
                        #If we already loaded a contig, yield that contig and
                        #start loading a new one
                        if contig:
                            yield([contig, seq]) #yield
                            contig = "" #reset contig and seq
                            seq = ""
                        split_line = line.split()
                        contig = (split_line[0].replace(">", ""))
                    else:
                        seq += line
                #Iyield last sequence, if it has both a header and sequence
                if contig and seq:
                    yield([contig, seq])
            except IOError:
                print("Could not read file ", fas)
                sys.exit(1)
            finally:
                fileh.close()
    else:
        raise FileNotFoundError("File %s not found!"%fas)

#Call main function
if __name__ == '__main__':
    main()
