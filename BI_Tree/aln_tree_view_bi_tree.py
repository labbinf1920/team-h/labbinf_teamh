#!/usr/bin/env python3

"""
This script is used to plot the nexus tree using
the MyRun01.con.tre
"""

import sys
import dendropy
from ete3 import Tree, TreeStyle, NodeStyle, TextFace
#import PyQt5


def remove_nexus_error(tree_file):
    """
       This function removes the caracter [&U] that would stop the tree from plotting
       :param tree_file: Tree file that will recieve changes
       :return: The tree file with the caracter [&U] removed
    """
    with open(tree_file, "r") as nexus_replace:
        remove = nexus_replace.read().replace("[&U]", "")
        with open(tree_file, "w+") as nexus_write:
            nexus_write.write(remove)

    return tree_file


def nexus_tree_visualization(tree_file):
    """
       This function allows the visualization of a Bayesian Tree
       :param tree_file: Tree file that will be used to plot a bayesin tree
    """
    # -------------------------------------------------------------------------
    # Convert to the format that is supported by ete3
    nexus_conv = dendropy.Tree.get(path=tree_file, schema="nexus")
    nexus_conv .write(path="pythonidae.newick", schema="newick")
    tree = Tree("pythonidae.newick", quoted_node_names=True, format=1)
    # Changes the aspect of the tree by using certain options
    tree_styling = TreeStyle()
    # -------------------------------------------------------------------------
    # Adds the title to the tree
    tree_styling.title.add_face(TextFace("Bayesian Tree", fsize=20), column=0)
    # -------------------------------------------------------------------------
    # Adds to the tree, a name to each branch
    tree_styling.show_leaf_name = True
    # -------------------------------------------------------------------------
    # Adds to the tree, the branch support -> Bootstrap values
    tree_styling.show_branch_support = True
    # -------------------------------------------------------------------------
    # Increases the trees lenght (Changes the branch lenght)
    tree_styling.scale = 100
    # -------------------------------------------------------------------------
    # Increase the separation of leaf branches
    tree_styling.branch_vertical_margin = 30
    # -------------------------------------------------------------------------
    # Redefines the aspect of the branches
    nstyle = NodeStyle()
    nstyle["hz_line_width"] = 2
    nstyle["hz_line_type"] = 1
    nstyle["hz_line_color"] = "orangered"
    nstyle["vt_line_width"] = 2
    nstyle["vt_line_type"] = 0
    nstyle["vt_line_color"] = "black"
    # -------------------------------------------------------------------------
    # Redefines the aspect of all the nodes in the tree
    for node in tree.traverse():
        nstyle["size"] = 5 # Size
        nstyle["fgcolor"] = "firebrick" # Colour
        node.set_style(nstyle)
    # -------------------------------------------------------------------------
    # Renderizes to an image file -> png, pdf or svg
    tree.render("mytree.png", tree_style=tree_styling, dpi=500, h=1920, w=1080, units='px')
    tree.render("mytree.svg", tree_style=tree_styling, dpi=500, h=1920, w=1080, units='px')
    # h - height
    # w - width

    return 0


def main():
    """ Main Function """
    tree_main_file = sys.argv[1]
    remove_nexus_error(tree_main_file)
    nexus_tree_visualization(tree_main_file)


if __name__ == "__main__":
    main()
