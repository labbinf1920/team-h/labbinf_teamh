#!/usr/bin/env python3

#Imports the script and filecmp module to compare files
import filecmp
from BI_Tree.aln_tree_view_bi_tree import nexus_tree_visualization


def test_nexus_tree_visualization():
    """
    Tests if trees produced with ML_tree_draw() match the mockups
    """
    # Produce a tree from a known mockup dataset:
    nexus_tree_visualization("tests/data/MyRun01.con.tre")

    # Assert if the produced tree is the same as mockup one
    assert filecmp.cmp("mytree.svg", "tests/data/mytree_bsn.svg",
                       shallow=False)
