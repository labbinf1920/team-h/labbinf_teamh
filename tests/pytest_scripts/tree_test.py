#!/usr/bin/env python3

#Imports the ml_tree_visualization script and filecmp module
import filecmp
from ML_Trees.ML_tree_draw import ml_tree_visualization


def test_Ml_tree_draw():
    """
    Tests if trees produced with ML_tree_draw() match the mockups
    """
    # Produce a tree from a known mockup dataset:
    ml_tree_visualization("ML_Trees/teste_C/RAxML_bipartitions.Multigene_tree_out_raxml")

    # Assert if the produced tree is the same as mockup one
    assert filecmp.cmp("mytree.svg", "tests/data/mytree.svg",
                       shallow=False)
