#!/usr/bin/env python3
#Imports the data_from_ncbi script

from ncbi_data.obtain_species_ncbi import data_from_ncbi

#Tests the first output

#Tranforms the dictionary output into string
output_test_1 = str(data_from_ncbi('taxonomy','Timon Lepidus'))

#Tests the existence of the following words on the string
def test_data_from_ncbi():
    l = ["TaxaSet","Taxon","ScientificName","Timon lepidus","OtherNames","Synonym","CommonName","ParentTaxId","Rank","Division","GeneticCode"]
    for element in l:
        assert element in output_test_1


#Tests the second output
#Converts the dictionary output into string
output_test_2 = str(data_from_ncbi('taxonomy','Timon [organism]'))
#Tests the existence of the following words on the string
def test_data_from_ncbi_test_2():
    l_2 = ["TaxaSet","Taxon","ScientificName","Timon lepidus","OtherNames","ParentTaxId","GeneticCode","MitoGeneticCode"]
    for element in l_2:
        assert element in output_test_2

 
