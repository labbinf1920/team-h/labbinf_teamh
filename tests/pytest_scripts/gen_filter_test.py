#!/usr/bin/env python3
#Imports the scripts from the gen_filter function for testing
from Bio import Entrez

from filter_taxa_genes.gene_filter import add_gene
from filter_taxa_genes.gene_filter import first_or_last_sequence
from filter_taxa_genes.gene_filter import shorter_or_longer_sequence
from filter_taxa_genes.gene_filter import get_all_gene_organism
from filter_taxa_genes.gene_filter import get_sequence_from_accession_number
from filter_taxa_genes.gene_filter import get_sequence
Entrez.email = "rubenaalbquerque@hotmail.com"

#tests the add_gene function
'''
def test_add_gene():
    l = ["ID: MF684987.1", "Name: MF684987", "Deion: Psammodromus microdactylus isolate 25232 proopiomelanocortin (pomc) gene, partial cds", "Number of features: 4",  "/molecule_type=DNA", "/topology=linear", "/data_file_division=VRT", "/date=11-OCT-2017", "/accessions=['MF684987']", "/sequence_version=1", "/keywords=['']", "/source=Psammodromus microdactylus",  "/organism=Psammodromus microdactylus", "/taxonomy=['Eukaryota', 'Metazoa', 'Chordata', 'Craniata', 'Vertebrata', 'Euteleostomi', 'Lepidosauria', 'Squamata', 'Bifurcata', 'Unidentata', 'Episquamata', 'Laterata', 'Lacertibaenia', 'Lacertidae', 'Psammodromus']", "/references=[Reference(title='Biogeographic crossroad across the Pillars of Hercules: Evolutionary history of Psammodromus lizards in space and time', ...), Reference(title='Direct Submission', ...)]", "/structured_comment=OrderedDict([('Assembly-Data', OrderedDict([('Sequencing Technology', 'Sanger dideoxy sequencing')]))])", "Seq('CCGTCGGCCCGAGGCGGCTGGGCAGGGGTCGGCGAGGGCGAGCCGGAGTCGGCC...TCT', IUPACAmbiguousDNA())"]
    x = '\n'.join(l)

    assert add_gene('gene','{}',"['source', 'gene', 'mRNA', 'CDS']",x) == {'pomc': [['MF684987.1', 0, 425]]}
'''


#Tests the output of the first_or_last_sequence function
def test_first_or_last_sequence():
    assert first_or_last_sequence({'pomc': [['MF684987.1', 0, 425]], 'nd4': [['MF684966.1', 0, 650], ['MF684965.1', 0, 650], ['MF684964.1', 0, 650]], 'mc1r': [['MF684940.1', 0, 610], ['MF684939.1', 0, 610], ['MF684938.1', 0, 610]], 'cytb': [['MF684921.1', 0, 392], ['MF684920.1', 0, 392], ['MF684919.1', 0, 392]], 'acm4': [['MF684899.1', 0, 357], ['MF684898.1', 0, 357]], '12S ribosomal RNA': [['MF684876.1', 0, 359], ['MF684875.1', 0, 357], ['MF684874.1', 0, 357]]},-1) == {'pomc': ['MF684987.1', 0, 425], 'nd4': ['MF684964.1', 0, 650], 'mc1r': ['MF684938.1', 0, 610], 'cytb': ['MF684919.1', 0, 392], 'acm4': ['MF684898.1', 0, 357], '12S ribosomal RNA': ['MF684874.1', 0, 357]}


#test the output of the shorter_or_longer_sequence function
def test_shorter_or_longer_sequence():
    assert shorter_or_longer_sequence({'pomc': [['MF684987.1', 0, 425]], 'nd4': [['MF684966.1', 0, 650], ['MF684965.1', 0, 650], ['MF684964.1', 0, 650]], 'mc1r': [['MF684940.1', 0, 610], ['MF684939.1', 0, 610], ['MF684938.1', 0, 610]], 'cytb': [['MF684921.1', 0, 392], ['MF684920.1', 0, 392], ['MF684919.1', 0, 392]], 'acm4': [['MF684899.1', 0, 357], ['MF684898.1', 0, 357]], '12S ribosomal RNA': [['MF684876.1', 0, 359], ['MF684875.1', 0, 357], ['MF684874.1', 0, 357]]},-1) == {'pomc': ['MF684987.1', 0, 425], 'nd4': ['MF684966.1', 0, 650], 'mc1r': ['MF684940.1', 0, 610], 'cytb': ['MF684921.1', 0, 392], 'acm4': ['MF684899.1', 0, 357], '12S ribosomal RNA': ['MF684876.1', 0, 359]}


#tests the output of the get_all_gene_organism function
def test_get_all_gene_organism():
    assert get_all_gene_organism('Psammodromus microdactylus','s') == {'pomc': ['MF684987.1', 0, 425], 'nd4': ['MF684966.1', 0, 650], 'mc1r': ['MF684940.1', 0, 610], 'cytb': ['MF684921.1', 0, 392], 'acm4': ['MF684899.1', 0, 357], '12S ribosomal RNA': ['MF684876.1', 0, 359]}


#test the output of the function get_sequence_from_accession_number
def test_get_sequence_from_accession_number():
    assert get_sequence_from_accession_number('pomc') == 'pomc'


#tests the output of the get_sequence function
def test_get_sequence():
    assert get_sequence('MF684876.1',0,359) == 'TCCGCCAGAGAACTACAAGTGAAAAACTTAAAACTCAAAGGACTTGGCGGTGTCCCACATTCAATCTAGAGGAGCCCGTCCTATAATCGATACTCCCCGCTTTACCTAACCCCAACTAGCATTCTAACTCAGCCTATATACCGCCGTCGACAGCTTACCCTCTGAGAGGTACAACAGTAAGCACAACAGCCCCCACTAACACGTCAGGTCAAGGTGTAGCTAATGTTGGGGTAGAGATTGGCTACATTTTTAAAATTTAAAAAAACGAAATGTTTCATGAAAAACAACACGAAGGCGAATTTAGTAGTAAAACAGACAAGAGAGTCTGCTTTAACAATGCTCTGGGACGCGCACACACC'

