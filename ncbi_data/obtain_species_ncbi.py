#!/usr/bin/env python3

'''
The main objetive of this script is to obtain all
necessary data from a specie, using the given name and the wanted rank
'''

import argparse
import json
import requests
import xmltodict


def data_from_ncbi(dbase, term):
    '''
    This will get all information needed from the NCBI database using NCBI's API.
    :param db: Databae to get the information from
    :param term: Term to search on the NCBI Database
    :return: A json file containing all the necessary information
    '''
    getinfo_ncbi = requests.get(
        'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?' + 'db='
        + dbase + '&' + 'term=' + term + '&usehistory=y&api_key=7caf53e1e5def3f9297371621cabd1faca09').text

    web_key = '&WebEnv=' + \
              (getinfo_ncbi[getinfo_ncbi.find('<WebEnv>')
                            + 8:getinfo_ncbi.find('</WebEnv>')])

    query_key = '&query_key=' \
                + (getinfo_ncbi[getinfo_ncbi.find('<QueryKey>')
                                + 10:getinfo_ncbi.find('</QueryKey>')])

    getdata_ncbi = requests.get('https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?'
                                + 'db=taxonomy' + web_key + query_key + '&api_key=7caf53e1e5def3f9297371621cabd1faca09').text

    json_data_fromxml = json.loads(json.dumps(xmltodict.parse(getdata_ncbi)))

    return json_data_fromxml


def get_initial_data(specie_name):
    '''
    This will get all information from the specie that the user wanted.
    :param specie_name: The name of the specie
    :return: return a string with all information
    '''
    initial_data = data_from_ncbi(dbase='taxonomy', term=specie_name)

    taxon_data = initial_data['TaxaSet']['Taxon']['LineageEx']['Taxon']

    return taxon_data


def get_allspecies_rank(data_taxon, rank):
    '''
    This will get all ranks and related name of a certain specie
    :param data_taxon: All data from the initial taxon
    :param rank: The rank that it's going to be selected
    :return: A list with all necessary species names
    '''
    dictionary_info = dict()

    for taxon in data_taxon:
        dictionary_info[taxon['Rank']] = taxon['ScientificName']

    other_data = data_from_ncbi(dbase='taxonomy', term=dictionary_info[rank] + ' [organism]')
    taxon_species = other_data['TaxaSet']['Taxon']

    list_species_name = []
    list_species_name_clones = []
    list_species_name_ignore = []

    list_species_checksubspecie = []

    for specie in taxon_species:
        specie_name = specie['ScientificName']
        if len(specie_name.split(" ")) > 5:
            list_species_name_ignore.append(specie_name)
        elif len(specie_name.split(" ")) == 4:
            list_species_name_clones.append(specie_name)
        elif len(specie_name.split(" ")) == 3:
            list_species_checksubspecie.append(specie_name)
            list_species_name.append(specie_name)
        elif len(specie_name.split(" ")) == 1 or "unclassified" in specie_name:
            pass
        else:
            list_species_name.append(specie_name)

    for subspecie in list_species_checksubspecie:
        for specie in list_species_name:
            if specie in subspecie:
                if specie == subspecie:
                    pass
                else:
                    list_species_name.pop(list_species_name.index(subspecie))
            else:
                pass


    for specie in list_species_name_clones:
        if (' '.join(specie.split(" ")[:3])) in list_species_name:
            pass
        else:
            if specie in list_species_name:
                pass
            else:
                list_species_name.append(specie)

    return list_species_name


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Obtains the initial data")

    parser.add_argument("--speciename", "-sn", type=str, help="Especie name")
    parser.add_argument("--rank", "-r", type=str, help="Rank to analyze")

    args = parser.parse_args()

    SPECIES_NAME = args.speciename
    RANKS = args.rank

    FINAL_SPECIES_NAMES = get_allspecies_rank(get_initial_data(SPECIES_NAME), RANKS)

    print(FINAL_SPECIES_NAMES)

