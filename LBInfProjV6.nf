#!/usr/bin/env nextflow

echo true
params.level = ""
params.species = ""
params.cores = 2
params.runs = 1100
params.treshO = 0.8
params.treshG = 0.7
params.ngen = 1000
params.alg = "ML"
species= ""
nrFiles = 0
listSize= 0
//params.opcaoRuben = "b"
/*
*Folder where the unaligned sequences are stored
*/
unalignedFolder = Channel.fromPath("./Files/unaligned/*.fasta")

/*
*Folder where the aligned sequences are stored
*/
aligned_seqs = Channel.fromPath("./Files/aligned/*.fasta")

/*
*Scrip that gets the species
*/
ncbiSeq = file('./ncbi_data/obtain_species_ncbi.py')

/*
*Script to filter the taxa
*/
filter = file("./filter_taxa_genes/gene_filter.py")

/*
*Script to run ML anylisis
*/
treescript = file("./ML_Trees/ml_tree_draw.py")

/*
*Script to concatenate Fasta files
*/
concatenator = file("./ML_Trees/teste_C/catfasta2phyml.pl")

/*
*Folder where MB related scripts are
*/
catFasta = file("./BI_Tree/concat_fasta.py")
mrbayes = file("./BI_Tree/aln_tree_view_bi_tree.py")

fastanexus = file("./fasta_to_nexus/main.py")

/*
*
*
*----------------------------------------------Inicio da Pipeline---------------------------------------------------------------------
*
*/

process scriptcare{

	input:
	file 'obtain_species_ncbi.py' from ncbiSeq 

	output:
	stdout channel

    	script:
	if ("$params.species" == "")
		error "Species not provided, specify one by --species 'value'"

	else if ("$params.level" == "")
				
		error "Species taxonomic rank not provided, specify one by --level 'value'"
	
			
	else
		"""               
  		
		python3 obtain_species_ncbi.py --speciename "${params.species}" --rank "${params.level}" 
    		"""	
}


process taxaFilter{

	publishDir './Files/unaligned/' , mode: 'copy'

	

	 
	input:
	file "gene_filter.py" from filter
	stdin channel
	
	output:
	path "*.fasta" into una


	script:
	"""	
	python3 gene_filter.py -org "${params.species}" -l "$channel.value" -po ${params.treshO} -pg ${params.treshG}
	"""
	
	
}



process aligner {
    
    publishDir './Files/aligned' , mode: 'copy'
	
    input:
    each x from una
    
 
    output:
    file "aln_${x.name}" into alignments
    //file "${x}_aln.fasta" into alignmentsMB
    

    script:
	
		   			
	""" 			   
	mafft --auto "${x}" > "aln_${x.name}"
	"""
	    	
}




if (params.alg=="MB"){

	process concatenatePY {
	   
	   publishDir './Files/FastaCat' , mode: 'copy'

	   input:
	   file "concat_fasta.py" from catFasta
	   path x from alignments.collect()

	   output:
	   file 'concat.fasta' into concatenatedpy
	
	   """
	   python3 concat_fasta.py $x
	   """
	}

	process nexusConverter {

	   

	   input:
	   file "concat.fasta" from concatenatedpy
	   file "main.py" from fastanexus

	   output:
	   file "nexus_file.nex" into buildermb

	   """
	   python3 main.py concat.fasta ${params.ngen}
	   """

	}


	process treebuilderMB {

	   publishDir './Files/MBoutput' , mode: 'copy'
	   
	   input:
	   file "nexus_file.nex" from buildermb

	   output:
	   file "MyRun01.con.tre" into treemb

	   """
	   mb nexus_file.nex
	   """

	}


	
	process treeviewerMB {
		
		publishDir './Files/Tree' , mode: 'copy'
	
		input:
		file "MyRun01.con.tre" from treemb
	    file "aln_tree_view_bi_tree.py" from mrbayes
	    

		output:
		file "mytree.png"
			
		"""
		python3 "aln_tree_view_bi_tree.py" "MyRun01.con.tre"
		"""
	
	}
	


}
else if (params.alg == "ML"){



	process concatenatePL {
		
		//errorStrategy 'finish'
		publishDir './Files/catedBI' , mode: 'copy'

		input:
		path x from alignments.collect()

		output:
		file 'genes_alnML.fasta' into concatenatedpl
		


		script:
		

		"""
		perl $concatenator -c $x > genes_alnML.fasta
		"""
		

	}

	process treebuilderMl {

		publishDir './Files/MLoutput' , mode: 'copy'         

		input:
		path 'genes_alnML.fasta' from concatenatedpl

		output:
		file "RAxML_bipartitions.seq_aln_out_raxml" into treeml

		"""
		raxmlHPC-PTHREADS-SSE3 -T ${params.cores} -f a -m GTRCAT -p 258369 -x 258369 -N ${params.runs} -s "genes_alnML.fasta" -n seq_aln_out_raxml 
		"""
	}




	process treeviewerML {
		
		publishDir './Files/Tree' , mode: 'copy'

		input:
		file "RAxML_bipartitions.seq_aln_out_raxml" from treeml	
		
		output:
		file "ML_tree.png"
	
		"""
		python3 $treescript RAxML_bipartitions.seq_aln_out_raxml
		"""

	}

}
else{
	
	error "Invalid algoritm : '${params.alg}'\nPlease specify either MB for MrBayes or ML for Maximum Likelyhood" 
	
}



