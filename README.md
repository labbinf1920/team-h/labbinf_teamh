# Project
-> Mega filogenias a partir de Bases de dados publicas

## Objetivos
* Determinar a posição de um taxa numa filogenia de um grupo a determinar;
	* Escolher o taxa que queremos estudar
		* Obter os genes
	* Escolher o grupo que queremos verificar
		* Obter os genes
	* ***Obter todos os genes do taxa e fazer uma árvore do mesmo grupo***
	* Filtrar genes comuns
	* Alinhamentos
	* Árvores filogenéticas
		* Análise biológica da árvore
	* Análise biológica do dataset e de tudo o resto
	* Ao fazer, comecar com analises de poucos genes para testar, e ir aumentando o numero de genes até ser um valor grande.

## Uso
* Download/update da imagem: `Sudo docker pull esteves198/teamh:007`
* Iniciar a imagem: `docker container run -v /home/(username)/labbinf_teamh/docker_output/:/outside --name (container name) -i -t esteves198/teamh:007 /bin/bash`
* Correr a pipeline (cada variável (abaixo) tem que ser separada por espaços (i.e): `~/nextflow --species "Timon Lepidus" --level "genus" --runs 10 --alg "ML"`
    * Espécie (**obrigatório!**): `--species "variavel"`
        * Variável **obrigatória**
        * Tipo de variável: **texto** (string)
    * Nível taxonómico (**obrigatório!**): `--level "variavel"`
        * Variável **obrigatória**
        * Tipo de variável: **texto** (string)
    * Número de cores: `--cores variavel`
        * Tipo de variável: **número** (int)
    * Número de runs: `--runs variavel`
        * Tipo de variável: **número** (int)
    * Trashold do organismo: `--treshO variavel`
        * Tipo de variável: **número** (float)
    * Trashold do gene: `--treshG variavel`
        * Tipo de variável: **número** (float)
    * Ngen: `--ngen variavel`
        * Tipo de variável: **número** (int)
    * Algoritmo: `--alg "variavel"`
        * Tipo de variável: **texto** (string)
        * Apenas existe **MB** e **ML**

* Os ficheiros das árvores são colocados na pasta Files/Tree.